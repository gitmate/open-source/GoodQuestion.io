---
layout: page
title: Team
permalink: /team/
---

We are a diverse team compiled out of developers and founders.

Here's a couple of us:

<div class="row">
  <div class="col s12 m4">
    <a href="https://www.linkedin.com/in/lasse-schuirmann-5395a7123/" target="_blank">
      <div class="author">
        <img src="../assets/img/team/Lasse.png" alt="Lasse">
        <div class="author-details">
          <div class="name">Lasse Schuirmann</div>
          <div class="position">Development</div>
        </div>
      </div>
    </a>
    <p>
      After his computer science studies, Lasse founded
      <a href="https://viperdev.io/">ViperDev.io</a> as well as
      <a href="https://gitmate.io/" target="_blank">GitMate.io</a> to help startups and enterprises
      build better software.
    </p>
  </div>
  <div class="col s12 m4">
    <a href="https://www.linkedin.com/in/sebastian-latacz-246882b7/" target="_blank">
      <div class="author">
        <img src="../assets/img/team/Sebastian.jpg" alt="Sebastian">
        <div class="author-details">
          <div class="name">Sebastian Latacz</div>
          <div class="position">Business Development</div>
        </div>
      </div>
    </a>
    <p>
      Sebastian combines business know-how and technical expertise.
      Customer Feedback helps him to make our product match
      market expectations.
    </p>
  </div>
  <div class="col s12 m4">
    <a href="https://www.linkedin.com/in/fabian-neuschmidt-508b53159/" target="_blank">
      <div class="author">
        <img src="../assets/img/team/Fabian.jpg" alt="Fabian">
        <div class="author-details">
          <div class="name">Fabian Neuschmidt</div>
          <div class="position">Data Scientist</div>
        </div>
      </div>
    </a>
    <p>
    Fabian trains our machine learning models to classify customer requests.
    He specialises in NLP and text-based machine learning.
    </p>
  </div>
</div>

If you want to meet us in person at our Hamburg office, shoot us an email at
<a href="mailto:contact@viperdev.io">contact@viperdev.io</a>.
