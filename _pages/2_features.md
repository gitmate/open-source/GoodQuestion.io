---
layout: page
title: Features
permalink: /features/
---

## Bots Are Broken

Chatbots don't work. We all know it. GoodQuestion does not pretend to be a human
but intelligently serves useful information.

## Receive Help Immediately

GoodQuestion is your 24/7 receptionist for all textual support requests.

GoodQuestion helps users with trivial questions immediately by suggesting them
knowledgebase articles in email or chat.

Your team responds to questions that matter to your business while customers receive much faster help with easy problems.
